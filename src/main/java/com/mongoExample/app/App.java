package com.mongoExample.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mongoExample.model.Employee;
import com.mongoExample.repositories.EmployeeRepository;
import com.mongoExample.serviceImpl.EmployeeServiceImpl;

import java.util.List;

public class App {
	 public static void main(String[] args) {
		 ApplicationContext context = new ClassPathXmlApplicationContext("spring-beans.xml");
		 
		 EmployeeServiceImpl employeeDaoImpl = context.getBean(EmployeeServiceImpl.class);
	  
		 //create employee Jessy Je
		 Employee employee_jessy_Je = new Employee("1", "Jessy", "Je", 21);
  
		 //create employee Jessy Man
		 Employee employee_jessy_Man = new Employee("2", "Jessy", "Man", 23);
  
		 //create employee Albert Johnson
		 Employee employee_albert_Johnson = new Employee("3", "Albert", "Johnson", 22);
	  
	  
		 employeeDaoImpl.create(employee_jessy_Je);
		 employeeDaoImpl.create(employee_jessy_Man);
		 employeeDaoImpl.create(employee_albert_Johnson);

		 System.out.println("===== find by first name 'Jessy' =====");
		 List<Employee> employeeList = employeeDaoImpl.findByFirstName("Jessy");
		 for(Employee employee : employeeList) {
			 System.out.println(employee);
		 }
	  
	  
		 System.out.println("\n===== find all=====");
		 employeeList = employeeDaoImpl.findAll();
		 for(Employee employee : employeeList) {
			 System.out.println(employee);
		 }
	  
		 System.out.println("\n===== find by age '22' =====");
		 employeeList = employeeDaoImpl.findByAge(22);
		 for(Employee employee : employeeList) {
			 System.out.println(employee);
		 }
	  
		 System.out.println("\n===== get employee from employeeCustomRepository =====");
		 employeeList = employeeDaoImpl.getEmployeeWithAgeGreaterThanEqual(22);
		 for(Employee employee : employeeList) {
			 System.out.println(employee);
		 }
	 }
}


// tutorial buat mongo properties
// https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#reference

// tutorial penggunaan JPA
// https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#repositories.create-instances.spring