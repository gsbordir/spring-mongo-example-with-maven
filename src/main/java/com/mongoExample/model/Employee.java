package com.mongoExample.model;
import java.io.Serializable;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Employee implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;
	private String firstName;
	private String lastName;
	private int age;
	
	public String getLastName() {
		return lastName;
	}

	public Employee(String id, String firstName, String lastName, int age) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public String getId() {
	  return id;
	}
	public String getFirstName() {
	  return firstName;
	}
	public int getAge() {
	  return age;
	}
	
	@Override
	public String toString() {
	  return "Person [id=" + this.id + ", name=" + this.firstName + ", age=" + this.age + "]";
	}
}