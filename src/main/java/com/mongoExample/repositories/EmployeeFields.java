package com.mongoExample.repositories;

public interface EmployeeFields {
	public final String ID = "id";
	public final String FIRSTNAME = "firstName";
	public final String LASTNAME = "lastName";
	public final String AGE = "age";
}
