package com.mongoExample.repositories;

import java.util.List;

import com.mongoExample.model.Employee;

public interface EmployeeRepositoryCustom {
	public List<Employee> findEmployeeWithAgeGreaterThanEqual(int age);
}
