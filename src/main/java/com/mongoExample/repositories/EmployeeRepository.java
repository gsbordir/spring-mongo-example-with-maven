package com.mongoExample.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.mongoExample.model.Employee;

public interface EmployeeRepository extends MongoRepository<Employee, String>{
	public void deleteById(String id);
	public List<Employee> findByFirstName(String firstName);
	public List<Employee> findByAge(int age);
}
