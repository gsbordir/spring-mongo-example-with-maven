package com.mongoExample.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongoExample.model.Employee;
import com.mongoExample.repositories.EmployeeRepositoryCustom;
import com.mongoExample.repositories.EmployeeRepository;
import com.mongoExample.service.employeeService;

@Service
public class EmployeeServiceImpl implements employeeService{
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private EmployeeRepositoryCustom employeeCustomRepository;
	
	@Override
	public Employee create(Employee employee) {
		if(employee != null) {
			employee = this.employeeRepository.save(employee);
		}
		return employee;
	}

	@Override
	public List<Employee> findByFirstName(String name) {
		return this.employeeRepository.findByFirstName(name);
		
	}

	@Override
	public void deleteById(String id) {
		this.employeeRepository.deleteById(id);
	}

	@Override
	public Employee update(Employee employee) {
		if(employee != null) {
			employee = this.employeeRepository.save(employee);
		}
		return employee;
	}

	@Override
	public List<Employee> findAll(){
		return this.employeeRepository.findAll();
	}

	@Override
	public List<Employee> findByAge(int age){
		return this.employeeRepository.findByAge(age);
	}
	
	@Override
	public List<Employee> getEmployeeWithAgeGreaterThanEqual(int age){
		return this.employeeCustomRepository.findEmployeeWithAgeGreaterThanEqual(22);
	}
}
