package com.mongoExample.service;

import java.util.List;

import com.mongoExample.model.Employee;

public interface employeeService {
	public Employee create(Employee employee);
	public List<Employee> findByFirstName(String name);
	public void deleteById(String id);
	public Employee update(Employee employee);
	public List<Employee> findAll();
	public List<Employee> findByAge(int age);
	public List<Employee> getEmployeeWithAgeGreaterThanEqual(int age);
}
