package com.mongoExample.RepositoryImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.mongoExample.model.Employee;
import com.mongoExample.repositories.EmployeeFields;
import com.mongoExample.repositories.EmployeeRepositoryCustom;

@Service
public class EmployeeRepositoryCustomImpl implements EmployeeRepositoryCustom {
	@Autowired
	MongoTemplate mongoTemplate;
	
	@Override
	public List<Employee> findEmployeeWithAgeGreaterThanEqual(int age) {
		Query query = new Query();
		query.addCriteria(Criteria.where(EmployeeFields.AGE).gte(22));
		List<Employee> employeeList = this.mongoTemplate.find(query, Employee.class);
		return employeeList;
	}

}
